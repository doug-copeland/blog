Given /^that I am on "(.*?)"$/ do |path|
  visit path
end

Then /^I should see "([^"]*)"$/ do |text|
  expect(page).to have_content text
end

When(/^I create a new post with title "(.*?)"$/) do |arg1|
  FactoryGirl.create(:post)
end

When /^I visit the page for the User$/ do
  expect (User.count) == 1
  visit(user_path(User.first))
end