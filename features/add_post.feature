Feature: Add Post

  Creating a new post should be quick and friendly.

  Scenario: View post page
    Given that I am on "http://localhost:3000/posts"
    Then I should see "New Post"

#  Scenario: Create new post
#    Given that I am on "http://localhost:3000/posts"
#    When I create a new post with title "New DAC Post"
#      And the body "This is a new post test message"
#    Then I should see the message "Post was successfully created."