require 'factory_girl'

FactoryGirl.define do
  factory :post do |f|
    f.title 'this is the testtitle'
    f.body 'this is the testbody'
  end
end